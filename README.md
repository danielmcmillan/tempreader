# TempReader

Reads temperature from 1-Wire DS18B20 sensor devices on the Raspberry Pi, and provides this data to RRDTool.